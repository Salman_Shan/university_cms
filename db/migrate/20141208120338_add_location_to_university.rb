class AddLocationToUniversity < ActiveRecord::Migration
  def change
    add_column :universities, :location, :text
  end
end
