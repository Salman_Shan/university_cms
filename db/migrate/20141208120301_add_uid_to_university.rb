class AddUidToUniversity < ActiveRecord::Migration
  def change
    add_column :universities, :uid, :string
  end
end
