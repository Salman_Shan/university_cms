class UniversitiesController < ApplicationController

	before_action :authenticate_user!

def index
  @university = University.all
end

def new

	@university = University.new
	
end

def show

	 @university = University.find(params[:id])
	
end

def create

	 @university = University.new(university_params)
 
  if @university.save
  redirect_to @university
  else
    render 'new'
  end

	
end

def edit
  @university = University.find(params[:id])
end

def update
  @university = University.find(params[:id])
 
  if @university.update(university_params)
    redirect_to @university
  else
    render 'edit'
  end
end

def destroy
  @university = University.find(params[:id])
  @university.destroy
 
  redirect_to universities_path
end
private
  def university_params
    params.require(:university).permit(:uid, :name, :location)
  end

end
