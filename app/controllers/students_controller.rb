class StudentsController < ApplicationController

before_action :authenticate_user!
	 def create
    @university = University.find(params[:university_id])
    @student = @university.students.create(student_params)
    redirect_to university_path(@university)
  end
 
 def destroy
    @university = University.find(params[:university_id])
    @student = @university.students.find(params[:id])
    @student.destroy
    redirect_to university_path(@university)
  end

  private
    def student_params
      params.require(:student).permit(:sid, :name, :age, :photo)
    end
end
