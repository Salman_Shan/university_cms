class Student < ActiveRecord::Base

	belongs_to :university

	  has_attached_file :photo
	 do_not_validate_attachment_file_type :photo

   validates_uniqueness_of :sid, :message => "already exists"

end
