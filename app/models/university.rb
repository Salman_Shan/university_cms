class University < ActiveRecord::Base
	has_many :students

   validates_uniqueness_of :uid, :message => "already exists"

end
